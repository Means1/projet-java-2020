package fr.projetjava.common.network.packet;

import fr.projetjava.common.ArmeType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
/**
 * Appellé quand un client switch d'arme
 */
public class SwitchArmePacket extends Packet {

    private int id;
    private int idArme;
    private ArmeType type;
}
