package fr.projetjava.common.network.packet;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

/**
 * Appellé quand une entité a été tué
 */
public class DeathEntityPacket extends Packet {
    private int id, owner;
}
