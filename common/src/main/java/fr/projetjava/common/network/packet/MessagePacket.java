package fr.projetjava.common.network.packet;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
/**
 * Appellé quand un joueur parle avec une bulle de tchat
 */
public class MessagePacket extends Packet {

    private int id;
    private String message;
}
