package fr.projetjava.common.network;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.projetjava.common.network.packet.Packet;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;

public class PacketManager {

    ArrayList<Listener> listeners = new ArrayList<>();

    /**
     * POur enregistrer un listener
     *
     * @param packetListener
     */
    public void registerListener(Listener packetListener) {
        listeners.add(packetListener);
    }

    /**
     * Appellé quand un packet est recu
     *
     * @param packet
     */
    public void call(Packet packet) {
        //On va regarder tous les listeners qui ont été registers
        try {
            for (Listener listener : listeners) {
                Method[] declaredConstructor = listener.getClass().getDeclaredMethods();
                //On regarde parmis toutes les fonctions pour savoir s'ils ont l'annotation PacketHandler et qu'elle a le packet en question en argument
                for (Method method : declaredConstructor) {
                    if (method.isAnnotationPresent(PacketHandler.class)) {
                        Parameter[] parameters = method.getParameters();
                        if (parameters.length == 1 && parameters[0].getType().isAssignableFrom(packet.getClass())) {
                            try {

                                //On appelle la fonction
                                method.invoke(listener, packet);
                            } catch (IllegalAccessException | InvocationTargetException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {

        }
    }


    /**
     * Appellé pour convertir un packet en chaine de caractères
     *
     * @param packet le pcket
     * @return la chaine de retour
     */
    public String convert(Packet packet) {
        //Permet de convertir un packet en String
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            PacketJson packetJson = new PacketJson();
            packetJson.setClassName(packet.getClass().getName());
            packetJson.setPacket(objectMapper.writeValueAsString(packet));
            return objectMapper.writeValueAsString(packetJson);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Appellé pour convertir une chaine de caractères en packet
     *
     * @param string la chaine de caractères
     * @return le packet
     */
    public Packet convert(String string) {
        //Permet de convertir une chaine de carractère en packet
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            PacketJson packetJson = objectMapper.readValue(string, PacketJson.class);
            Class aClass = Class.forName(packetJson.getClassName());
            return (Packet) objectMapper.readValue(packetJson.getPacket(), aClass);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Getter
    @Setter
    @NoArgsConstructor
    public static class PacketJson{

        private String className;
        private String packet;

    }
}
