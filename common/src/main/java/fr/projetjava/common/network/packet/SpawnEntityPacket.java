package fr.projetjava.common.network.packet;

import fr.projetjava.common.entity.EntityType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
/**
 * Appellé pour faire spawn des entités
 */
public class SpawnEntityPacket extends Packet {
    private SpawnEntity[] spawnEntities;


    @AllArgsConstructor
    @NoArgsConstructor
    @Getter
    @Setter
    public static class SpawnEntity{
        private int id;
        private double x, y;
        private EntityType entityType;
        private int data;
    }
}
