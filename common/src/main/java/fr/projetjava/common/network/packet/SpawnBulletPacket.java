package fr.projetjava.common.network.packet;

import fr.projetjava.common.ArmeType;
import fr.projetjava.common.utils.MouvementDirection;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
/**
 * Appellé pour faire spanw des bullet sur la map
 */
public class SpawnBulletPacket extends Packet {

    private SpawnBullet[] spawnBullets;



    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    public static class SpawnBullet {

        private int id;
        private int owner;
        private ArmeType armeType;
        private MouvementDirection mouvementDirection;
    }
}
