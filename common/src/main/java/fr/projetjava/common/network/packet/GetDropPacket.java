package fr.projetjava.common.network.packet;

import fr.projetjava.common.Drop;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
/**
 * APpellé quand un joueur récupère un drop au sol
 */
public class GetDropPacket extends Packet {

    private int id;
    private int itemId;
    private Drop drop;
}
