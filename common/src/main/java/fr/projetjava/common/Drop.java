package fr.projetjava.common;

import lombok.Getter;

import java.util.Random;
import java.util.stream.Stream;

public enum Drop {

    ARME_SIMPLE(0, 0, ArmeType.SIMPLE), ARME_DOUBLE(10, 1, ArmeType.DOUBLE), ARME_TRIPLE(8, 2, ArmeType.TRIPLE), ARME_POMPE(5, 3, ArmeType.POMPE),
    ARME_FIREBALL(2, 4, ArmeType.FIREBALL), KEBAB(25, 5, null), KFC(10, 6, null),
    BIGMAC(25, 7, null), FORCE(5, 8, null), SPEED(5 , 9, null), XP(5, 10, null);

    int chance;
    @Getter
    private int id;
    @Getter
    private ArmeType type;

    Drop(int chance, int id, ArmeType type) {
        this.chance = chance;
        this.type = type;
        this.id = id;
    }

    public static Drop getByArme(ArmeType armeType){
        return Stream.of(values()).filter(p -> p.getType() == armeType).findFirst().orElse(null);
    }


    public static Drop random(){
        int i = new Random().nextInt(100);
        for (Drop value : values()) {
            i -= value.chance;
            if (i <= 0)
                return value;
        }
        return null;
    }

    public static Drop getById(int data) {
        return Stream.of(values()).filter(p -> p.getId() == data).findFirst().orElse(null);
    }

    public boolean isArme() {
        return type != null;
    }
}
