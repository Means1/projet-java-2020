package fr.projetjava.common.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MouvementDirection {

    private double x;
    private double y;
    private double speed;

    private Direction direction;

}
