package fr.projetjava.common.utils;

import fr.projetjava.common.ArmeType;
import fr.projetjava.common.network.packet.Packet;

public interface FactoryPacketArme {

    Packet process(ArmeType type, int id, double x, double y, double angle);

}
