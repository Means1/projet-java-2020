package fr.projetjava.common.utils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Direction {

    @Getter
    private double north;
    @Getter
    private double right;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Direction direction = (Direction) o;
        return Double.compare(direction.north, north) == 0 &&
                Double.compare(direction.right, right) == 0;
    }

    @JsonIgnore
    public boolean isNul() {
        return north == 0 && right == 0;
    }
}
