package fr.projetjava.client.manager;

import com.almasb.fxgl.dsl.FXGL;
import com.almasb.fxgl.ui.FontFactory;
import fr.projetjava.client.Main;
import fr.projetjava.client.entity.ClientEntity;
import fr.projetjava.client.entity.component.EntityComponent;
import fr.projetjava.client.entity.living.ennemi.EnnemiClientEntity;
import fr.projetjava.client.entity.living.player.PlayerClientEntity;
import fr.projetjava.client.entity.living.vip.VipClientEntity;
import fr.projetjava.client.entity.other.armes.BulletClientEntity;
import fr.projetjava.common.ArmeType;
import fr.projetjava.common.utils.Direction;
import fr.projetjava.common.utils.MouvementDirection;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.text.Text;
import lombok.Getter;
import lombok.Setter;

public class LaunchManager {

    @Setter
    @Getter
    private Text text;

    private EnnemiClientEntity entity;

    /**
     * Initialisation du text au milieu de l'écran qui affiche le compte a rebourd
     *
     * @return Node
     */
    public Node initText() {
        text = new Text();
        text.setTranslateX(FXGL.getAppWidth() / 2);
        text.setTranslateY(FXGL.getAppHeight() / 4);
        text.setText("...");

        FontFactory stocky = FXGL.getAssetLoader().loadFont("stocky.ttf");
        text.setFont(stocky.newFont(25));
        return text;
    }

    /**
     * démarrage de l'animation d'intro
     */
    public void startAnimation() {
        ClientEntity entity = Main.getInstance().getEntities().stream().filter(p -> p instanceof VipClientEntity).findFirst().orElse(null);
        FXGL.getGameScene().getViewport().unbind();
        FXGL.getGameScene().getViewport().bindToEntity(entity.getEntityGraphique(), FXGL.getAppHeight() / 2, FXGL.getAppWidth() / 2);
        entity.getEntityGraphique().getComponent(EntityComponent.class).setBulle("Bonjour, moi c'est Mr. George.\n\nle but du jeu est très simple ! \nIl suffit de me protéger\nde ces méchants zombies !");
        this.entity = new EnnemiClientEntity(-1, 2080, 2150, 2);
        new Thread(() -> {
            try {
                Thread.sleep(8000);
            } catch (InterruptedException e) {
            }
            Platform.runLater(() -> {
                entity.getEntityGraphique().getComponent(EntityComponent.class).setBulle("Pour me proteger,\nil faut tirer avec\nvotre arme grâce à la souris !\nQuand le zombie meurt,\nil peut donner des choses !");
            });
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
            }
            for (int i = 0; i < 5; i++) {
                Platform.runLater(() -> {
                    new BulletClientEntity(-1, ArmeType.SIMPLE, new MouvementDirection(2180, 2250, 1, new Direction(1, -1)), -1);
                });
                try {
                    Thread.sleep(250);
                } catch (InterruptedException e) {

                }
            }
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
            }
            Platform.runLater(() -> {
                entity.getEntityGraphique().getComponent(EntityComponent.class).setBulle("Tu peux me dire d'avancer\nou non avec ta touche ESPACE\nTu peux ramasser les items\navec E, tu peux te déplacer\navec Z, Q, S, D et tu peux\naussi utiliser les items à gauche.\nBonne chance !!");
            });
            try {
                Thread.sleep(8000);
            } catch (InterruptedException e) {
            }
            Platform.runLater(() -> {
                PlayerClientEntity mainEntity = Main.getInstance().getMainEntity();
                FXGL.getGameScene().getViewport().unbind();
                FXGL.getGameScene().getViewport().bindToEntity(mainEntity.getEntityGraphique(), FXGL.getAppHeight() / 2, FXGL.getAppWidth() / 2);
            });

        }).start();

    }
}
