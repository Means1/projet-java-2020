package fr.projetjava.client.manager;

import com.almasb.fxgl.dsl.FXGL;
import com.almasb.fxgl.ui.FontFactory;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import lombok.Getter;

public class SpectatorManager {

    @Getter
    private final Text text;
    private final Rectangle rectangle;
    private boolean spectator;

    /**
     * Création du SpectatorManger
     */
    public SpectatorManager() {
        rectangle = new Rectangle(FXGL.getAppHeight(), FXGL.getAppWidth(), new Color(0.6, 0.6, 0.6, 0.4));
        text = new Text();

        text.setTranslateX(FXGL.getAppWidth() / 2 - 220);
        text.setTranslateY(FXGL.getAppHeight() / 4);
        text.setText("Vous etes mort !");
        text.setFill(Color.RED);

        FontFactory stocky = FXGL.getAssetLoader().loadFont("stocky.ttf");
        text.setFont(stocky.newFont(50));
    }

    /**
     * Met le joueur en mode spectator
     */
    public void joinSpectator() {
        if (spectator) return;
        FXGL.getGameScene().addUINode(rectangle);
        FXGL.getGameScene().addUINode(text);
        spectator = true;
    }

    /**
     * Enleve le joueur du mode spectator
     */
    public void leftSpectator() {
        if (!spectator) return;
        FXGL.getGameScene().removeUINode(rectangle);
        FXGL.getGameScene().removeUINode(text);
        spectator = false;
    }
}
