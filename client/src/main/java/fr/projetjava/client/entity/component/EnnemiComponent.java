package fr.projetjava.client.entity.component;

import com.almasb.fxgl.dsl.FXGL;
import com.almasb.fxgl.entity.component.Component;
import com.almasb.fxgl.texture.AnimatedTexture;
import com.almasb.fxgl.texture.AnimationChannel;
import fr.projetjava.client.entity.living.ennemi.EnnemiClientEntity;
import javafx.util.Duration;
import lombok.Setter;

import java.util.Random;

public class EnnemiComponent extends Component {

    private final AnimatedTexture texture;
    private final AnimationChannel animWalkRight;
    @Setter
    private EnnemiClientEntity ennemiClientEntity;

    /**
     * Création de l'entitycomponent
     */
    public EnnemiComponent() {
        int i = new Random().nextInt(4);
        animWalkRight = new AnimationChannel(FXGL.getAssetLoader().loadImage("zombieSprite.png"), 4, 63, 63, Duration.seconds(1), i * 4, i * 4 + 3);

        texture = new AnimatedTexture(animWalkRight);
        texture.loop();
    }

    /**
     * Appellé lors de la création
     */
    @Override
    public void onAdded() {
        entity.getViewComponent().addChild(texture);
    }

    /**
     * A chaque update
     *
     * @param tpf
     */
    @Override
    public void onUpdate(double tpf) {
        if (ennemiClientEntity == null)
            return;
        if (ennemiClientEntity.getLastDirection().getRight() > 0 && entity.getScaleX() != 1) {
            entity.setScaleX(1);
        } else if (ennemiClientEntity.getLastDirection().getRight() < 0 && entity.getScaleX() != -1) {
            entity.setScaleX(-1);
        }
    }
}
