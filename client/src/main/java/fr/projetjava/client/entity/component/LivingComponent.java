package fr.projetjava.client.entity.component;

import com.almasb.fxgl.dsl.FXGL;
import com.almasb.fxgl.entity.Entity;
import com.almasb.fxgl.entity.component.Component;
import fr.projetjava.client.entity.living.LivingClientEntity;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import lombok.Setter;

@Setter
public class LivingComponent extends Component {

    @Setter
    private LivingClientEntity livingClientEntity;
    private Entity entity;
    private Entity entityBis;
    private int oldLife;
    private int size;

    /**
     * Création du living component
     *
     * @param size taille de la lifebar
     */
    public LivingComponent(int size) {
        this.size = size;
    }


    /**
     * Appellé lors de la création de l'entité
     */
    @Override
    public void onAdded() {
        entityBis = FXGL.spawn("unLifeBar", getEntity().getBoundingBoxComponent().getMinXWorld() + getEntity().getBoundingBoxComponent().getWidth() / 2 - size / 2, getEntity().getBoundingBoxComponent().getMinYWorld() - 5);
        entityBis.getViewComponent().addChild(new Rectangle(size, 3, Color.RED));
        entity = FXGL.spawn("lifeBar", getEntity().getBoundingBoxComponent().getMinXWorld() + getEntity().getBoundingBoxComponent().getWidth() / 2 - size / 2, getEntity().getBoundingBoxComponent().getMinYWorld() - 5);
        entity.getViewComponent().addChild(new Rectangle(size, 3, Color.GREENYELLOW));
    }

    /**
     * Appéllé quand l'entité est supprimé
     */
    @Override
    public void onRemoved() {
        if (entity.isActive())
            FXGL.getGameWorld().removeEntity(entity);
        if (entityBis.isActive())
            FXGL.getGameWorld().removeEntity(entityBis);

    }

    /**
     * Appellé à chaque update
     *
     * @param tpf
     */
    @Override
    public void onUpdate(double tpf) {
        entity.setPosition(getEntity().getBoundingBoxComponent().getMinXWorld() + getEntity().getBoundingBoxComponent().getWidth() / 2 - size / 2, getEntity().getBoundingBoxComponent().getMinYWorld() - 5);
        entityBis.setPosition(getEntity().getBoundingBoxComponent().getMinXWorld() + getEntity().getBoundingBoxComponent().getWidth() / 2 - size / 2, getEntity().getBoundingBoxComponent().getMinYWorld() - 5);
        if (oldLife != livingClientEntity.getCurrentLife()) {
            oldLife = livingClientEntity.getCurrentLife();
            entity.getViewComponent().clearChildren();
            int x = (int) (((double) oldLife / (double) livingClientEntity.getMaxLife()) * size);
            if (x < 0)
                x = 0;
            if (x > 0)
                entity.getViewComponent().addChild(new Rectangle(x,3,Color.GREENYELLOW));
        }
    }
}
