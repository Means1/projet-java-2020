package fr.projetjava.client.entity.other;

import fr.projetjava.client.entity.ClientEntity;
import fr.projetjava.common.entity.EntityType;

public class MapClientEntity extends ClientEntity {

    /**
     * Création d'une map entité
     *
     * @param id id
     */
    public MapClientEntity(int id) {
        super(EntityType.MAP, id);
    }
}
