package fr.projetjava.client.entity;

import com.almasb.fxgl.dsl.FXGL;
import fr.projetjava.client.Main;
import fr.projetjava.client.entity.component.EntityComponent;
import fr.projetjava.client.manager.MouvementManager;
import fr.projetjava.common.entity.EntityType;
import fr.projetjava.common.utils.Direction;
import fr.projetjava.common.utils.MouvementDirection;
import javafx.application.Platform;
import javafx.geometry.Point2D;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@NoArgsConstructor
public class ClientEntity {

    private EntityType type;
    @Setter
    private MouvementDirection mouvementDirection = new MouvementDirection();
    private int id;
    private com.almasb.fxgl.entity.Entity entityGraphique;
    @Setter
    private Point2D oldPos;
    @Setter
    private Direction lastDirection = new Direction(0, 1);
    @Setter
    private MouvementManager.Correction correction;

    /**
     * Création d'une entité client
     *
     * @param type Le type
     * @param id   l'id
     * @param x    le x
     * @param y    le y
     */
    public ClientEntity(EntityType type, int id, double x, double y) {
        this.id = id;
        this.type = type;
        this.mouvementDirection.setX(x);
        this.mouvementDirection.setY(y);
        this.oldPos = new Point2D(x, y);
        this.mouvementDirection.setDirection(new Direction(0, 0));
        this.mouvementDirection.setSpeed(1);
        entityGraphique = FXGL.spawn(type.getName(), x, y);
        getEntityGraphique().getComponent(EntityComponent.class).setClientEntity(this);
        Main.getInstance().getEntities().add(this);
    }

    /**
     * Création d'une entité client
     *
     * @param type type
     * @param id   id
     */
    public ClientEntity(EntityType type, int id) {
        this(type, id, 0, 0);
    }


    /**
     * On supprime l'entité
     */
    public void delete() {
        despawn();
        Main.getInstance().getEntities().remove(this);
    }

    /**
     * On despawn l'entité
     */
    private void despawn() {
        Platform.runLater(() -> {
            if (getEntityGraphique().isActive())
                FXGL.getGameWorld().removeEntities(getEntityGraphique());
        });
    }
}
