package fr.projetjava.client.network.listeners;

import com.almasb.fxgl.audio.Sound;
import com.almasb.fxgl.dsl.FXGL;
import fr.projetjava.client.Main;
import fr.projetjava.client.entity.ClientEntity;
import fr.projetjava.client.entity.component.EntityComponent;
import fr.projetjava.client.entity.living.LivingClientEntity;
import fr.projetjava.client.entity.living.player.PlayerClientEntity;
import fr.projetjava.client.ui.GameEntityFactory;
import fr.projetjava.common.network.Listener;
import fr.projetjava.common.network.PacketHandler;
import fr.projetjava.common.network.packet.*;
import javafx.application.Platform;
import javafx.geometry.Point2D;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

/**
 * Class qui sera un listener qui contient les fonctions qui seront appellé automatiquement.
 */
public class PacketListener implements Listener {

    @PacketHandler
    public void onPacketLogin(LoginPacket packet){
        Main.getInstance().setId(packet.getId());
    }

    @PacketHandler
    public void onPacketSpawnEntity(SpawnEntityPacket packet){
        Platform.runLater(() ->  {
            for (SpawnEntityPacket.SpawnEntity spawnEntity : packet.getSpawnEntities()) {
                GameEntityFactory.spawnEntity(spawnEntity.getEntityType(), spawnEntity.getId(), spawnEntity.getX(), spawnEntity.getY(), spawnEntity.getData());
            }
        });
    }

    @PacketHandler
    public void onPacketSwitchArme(SwitchArmePacket packet){
        ////Sound sound = FXGL.getAssetLoader().loadSound("switch.wav");
        //FXGL.getAudioPlayer().playSound(sound);
        if (packet.getId() == Main.getInstance().getId()) {
            PlayerClientEntity playerClientEntity = Main.getInstance().getMainEntity();
            if (playerClientEntity != null){
                Platform.runLater(() -> {
                    playerClientEntity.setArmeType(packet.getType());
                    playerClientEntity.updateArme();
                });
            }
        }
    }

    @PacketHandler
    public void onPacketMoveEntity(MoveEntityPacket packet){
        ClientEntity clientEntityById = Main.getInstance().getEntityById(packet.getId());
        if (clientEntityById != null){
            clientEntityById.setMouvementDirection(packet.getMouvementDirection());
            if (!packet.getMouvementDirection().getDirection().isNul())
                clientEntityById.setLastDirection(packet.getMouvementDirection().getDirection());
            clientEntityById.setOldPos(new Point2D(clientEntityById.getEntityGraphique().getX(), clientEntityById.getEntityGraphique().getY()));
        }
    }

    @PacketHandler
    public void onPacketRemoveEntity(RemoveEntityPacket packet){
        ////Sound sound = FXGL.getAssetLoader().loadSound("zombie.wav");
        //FXGL.getAudioPlayer().playSound(sound);
        ClientEntity entity = Main.getInstance().getEntityById(packet.getId());
        if (entity == null)
            return;
        Platform.runLater(entity::delete);
    }

    @PacketHandler
    public void onPacketLaunch(LaunchPacket packet){
        Platform.runLater(() -> {
            String textMessage = "";
            Text text = Main.getInstance().getLaunchManager().getText();
            switch (packet.getTime()) {
                case 5:
                case 4:
                case 3:
                    //Sound sound1 = FXGL.getAssetLoader().loadSound("bip.wav");
                    //FXGL.getAudioPlayer().playSound(sound1);
                    textMessage = "Attention !";
                    text.setFill(Color.RED);
                    break;
                case 2:
                    //Sound sound2 = FXGL.getAssetLoader().loadSound("bip.wav");
                    //FXGL.getAudioPlayer().playSound(sound2);
                    textMessage = "A vos marques....";
                    text.setFill(Color.ORANGE);
                    break;
                case 1:
                    //Sound sound3 = FXGL.getAssetLoader().loadSound("bip.wav");
                    //FXGL.getAudioPlayer().playSound(sound3);
                    textMessage = "Pret ? ";
                    break;
                case 0:
                    //Sound sound = FXGL.getAssetLoader().loadSound("start.wav");
                    //FXGL.getAudioPlayer().playSound(sound);
                    textMessage = "C'est parti !";
                    text.setFill(Color.GREEN);
                    Main.getInstance().getLaunchManager().startAnimation();
                    break;
                case -1:
                    FXGL.getGameScene().removeUINode(text);
                    return;
                default:
                    textMessage = "Demarrage dans " + packet.getTime() + " secondes !";
                    text.setFill(Color.DARKTURQUOISE);
            }
            text.setTranslateX(FXGL.getAppWidth()/2 - 7 * textMessage.length());
            text.setText(textMessage);
        });
    }


    @PacketHandler
    public void onSpawnBulletPacket(SpawnBulletPacket packet){
        //Sound sound = FXGL.getAssetLoader().loadSound("bullet.wav");
        //FXGL.getAudioPlayer().playSound(sound);
        Platform.runLater(() ->  {
            GameEntityFactory.spawnBullet(packet);
        });
    }

    @PacketHandler
    public void onGetDropPacket(GetDropPacket packet){
        if (packet.getId() == Main.getInstance().getId()) {
            PlayerClientEntity mainEntity = Main.getInstance().getMainEntity();
            if (mainEntity != null){
                mainEntity.addInventory(packet.getDrop());
            }
        }
    }

    @PacketHandler
    public void onDamagePacket(DamagePacket packet){
        ClientEntity entity = Main.getInstance().getEntityById(packet.getTarget());
        if (entity instanceof LivingClientEntity){
            //Sound sound = FXGL.getAssetLoader().loadSound("woosh.wav");
            //FXGL.getAudioPlayer().playSound(sound);
            ((LivingClientEntity) entity).damage(packet.getDamage());
        }
    }

    @PacketHandler
    public void onPointAddPacket(PointsAddPacket pointsAddPacket){
        if (pointsAddPacket.getId() == Main.getInstance().getId()){
            FXGL.getGameState().increment("points", pointsAddPacket.getPoints());
        }
    }

    @PacketHandler
    public void onHealdPacket(HealEntityPacket packet){
        //Sound sound = FXGL.getAssetLoader().loadSound("heal.wav");
        //FXGL.getAudioPlayer().playSound(sound);
        ClientEntity entity = Main.getInstance().getEntityById(packet.getId());
        if (entity instanceof LivingClientEntity){
            ((LivingClientEntity) entity).setCurrentLife(packet.getLife());
            if (packet.getId() == Main.getInstance().getId()){
                Main.getInstance().updateLife();
            }
        }
    }

    @PacketHandler
    public void onEndGame(EndGamePacket packet){
        if (packet.isKick()){
            Platform.runLater(FXGL.getGameController()::gotoMainMenu);
        } else {
            //Sound sound = FXGL.getAssetLoader().loadSound("cri.wav");
            //FXGL.getAudioPlayer().playSound(sound);
            Main.getInstance().getSpectatorManager().getText().setText("Vous avez perdu !");
            Platform.runLater(() -> {
                Main.getInstance().getSpectatorManager().joinSpectator();
            });
        }
    }

    @PacketHandler
    public void onMessagePacket(MessagePacket packet){
        ClientEntity entityById = Main.getInstance().getEntityById(packet.getId());
        EntityComponent component = entityById.getEntityGraphique().getComponent(EntityComponent.class);
        Platform.runLater(() -> {
            component.setBulle(packet.getMessage());
        });
    }
}
