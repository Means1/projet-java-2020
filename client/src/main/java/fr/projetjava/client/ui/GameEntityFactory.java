package fr.projetjava.client.ui;

import com.almasb.fxgl.dsl.FXGL;
import com.almasb.fxgl.entity.Entity;
import com.almasb.fxgl.entity.EntityFactory;
import com.almasb.fxgl.entity.SpawnData;
import com.almasb.fxgl.entity.Spawns;
import com.almasb.fxgl.physics.BoundingShape;
import com.almasb.fxgl.physics.HitBox;
import fr.projetjava.client.entity.component.*;
import fr.projetjava.client.entity.living.ennemi.EnnemiClientEntity;
import fr.projetjava.client.entity.living.player.PlayerClientEntity;
import fr.projetjava.client.entity.living.vip.VipClientEntity;
import fr.projetjava.client.entity.other.MapClientEntity;
import fr.projetjava.client.entity.other.armes.BulletClientEntity;
import fr.projetjava.client.entity.other.armes.DropClientEntity;
import fr.projetjava.common.ArmeType;
import fr.projetjava.common.entity.EntityType;
import fr.projetjava.common.network.packet.SpawnBulletPacket;
import fr.projetjava.common.utils.Direction;
import fr.projetjava.common.utils.MouvementDirection;
import javafx.geometry.Point2D;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class GameEntityFactory implements EntityFactory {

    /**
     * Lors de la réception d'un packet de spawn
     *
     * @param entityType Type de l'entité
     * @param id         son id
     * @param x          son x
     * @param y          son y
     * @param data       une donnée
     */
    public static void spawnEntity(EntityType entityType, int id, double x, double y, int data) {
        switch (entityType) {
            case MAP:
                new MapClientEntity(id);
                break;
            case PLAYER:
                new PlayerClientEntity(id, x, y);
                break;
            case VIP:
                new VipClientEntity(id, x, y);
                break;
            case ZOMBIE:
                new EnnemiClientEntity(id, x, y, data);
                break;
            case BULLET_SIMPLE:
                new BulletClientEntity(id, ArmeType.SIMPLE, new MouvementDirection(x, y, 0, new Direction()), 0);
                break;
            case DROP:
                new DropClientEntity(id, x, y, data);
                break;
        }
    }

    /**
     * Lors de la reception d'une SpawnBUlletPacket
     *
     * @param packet le packet
     */
    public static void spawnBullet(SpawnBulletPacket packet) {
        for (SpawnBulletPacket.SpawnBullet spawnBullet : packet.getSpawnBullets()) {
            new BulletClientEntity(spawnBullet.getId(), spawnBullet.getArmeType(), spawnBullet.getMouvementDirection(), spawnBullet.getOwner());
        }
    }

    /**
     * Création d'une entité player avec un {@link PlayerComponent}, {@link LivingComponent}, {@link EntityComponent}, avec des collisions et une hitbox
     *
     * @param data x, y
     * @return l'entité
     */
    @Spawns("player")
    public Entity newPlayer(SpawnData data) {
        return FXGL.entityBuilder()
                .type(EntityType.PLAYER)
                .from(data)
                .zIndex(2)
                .with(new PlayerComponent())
                .with(new LivingComponent(50))
                .with(new EntityComponent())
                .collidable()
                .bbox(new HitBox(new Point2D(16, 16), BoundingShape.box(32, 48)))
                .build();
    }

    /**
     * Création d'une entity fireball avec un {@link EntityComponent}, une hitbox, des collisions
     *
     * @param data x, y
     * @return l'entité
     */
    @Spawns("bullet_fireball")
    public Entity newBulletFireball(SpawnData data) {
        return FXGL.entityBuilder().from(data)
                .type(EntityType.BULLET_FIREBALL)
                .view("fireball.png")
                .bbox(new HitBox(BoundingShape.circle(16)))
                .with(new EntityComponent())
                .collidable()
                .zIndex(3)
                .build();
    }

    /**
     * Création d'une entity bullet avec un {@link EntityComponent}, une hitbox, des collisions et une texture de cercle noir
     *
     * @param data x, y
     * @return l'entité
     */
    @Spawns("bullet_simple")
    public Entity newBulletSimple(SpawnData data) {
        return FXGL.entityBuilder().from(data)
                .type(EntityType.BULLET_SIMPLE)
                .view(new Circle(3, Color.BLACK))
                .bbox(new HitBox(BoundingShape.circle(3)))
                .with(new EntityComponent())
                .collidable()
                .zIndex(3)
                .build();
    }

    /**
     * Création de l'entity map avec un {@link EntityComponent}, et une texture
     *
     * @param data x, y
     * @return la map
     */
    @Spawns("map")
    public Entity newMap(SpawnData data) {
        return FXGL.entityBuilder()
                .type(EntityType.MAP)
                .from(data)
                .with(new EntityComponent())
                .view("grand_cauchyMapUpdate.png")
                .build();
    }

    /**
     * Création de l'entity vip avec un {@link VipComponent}, {@link EntityComponent}, {@link LivingComponent}, une hitbox et des collisions
     *
     * @param data x, y
     * @return l'entité
     */
    @Spawns("vip")
    public Entity newVip(SpawnData data) {
        return FXGL.entityBuilder()
                .type(EntityType.VIP)
                .from(data)
                .zIndex(1)
                .collidable()
                .bbox(new HitBox(new Point2D(16, 0), BoundingShape.box(32, 48)))
                .with(new VipComponent())
                .with(new EntityComponent())
                .with(new LivingComponent(70))
                .build();
    }

    /**
     * Création de l'entity zombie avec un {@link EnnemiComponent}, {@link EntityComponent}, {@link LivingComponent}, une hitbox et des collisions
     *
     * @param data x, y
     * @return l'entité
     */
    @Spawns("zombie")
    public Entity newZombie(SpawnData data) {
        return FXGL.entityBuilder()
                .type(EntityType.ZOMBIE)
                .from(data)
                .zIndex(1)
                .with(new EnnemiComponent())
                .with(new EntityComponent())
                .with(new LivingComponent(50))
                .collidable()
                .bbox(new HitBox(new Point2D(10, 0), BoundingShape.box(40, 62)))
                .build();
    }

    /**
     * Création de la bar de vie avec un {@link EntityComponent}
     *
     * @param data x, y
     * @return la barre de vie
     */
    @Spawns("lifeBar")
    public Entity newLifeBar(SpawnData data) {
        return FXGL.entityBuilder()
                .from(data)
                .with(new EntityComponent())
                .zIndex(4)
                .build();
    }

    /**
     * Création de la bulle de tchat avec un {@link EntityComponent}
     *
     * @param data x, y
     * @return la barre de vie
     */
    @Spawns("bulle")
    public Entity newBulle(SpawnData data) {
        return FXGL.entityBuilder()
                .from(data)
                .with(new EntityComponent())
                .zIndex(4)
                .build();
    }

    /**
     * Création de la bar de vie avec un {@link EntityComponent}
     *
     * @param data x, y
     * @return la barre de vie (partie rouge)
     */
    @Spawns("unLifeBar")
    public Entity newUnLifeBar(SpawnData data) {
        return FXGL.entityBuilder()
                .from(data)
                .with(new EntityComponent())
                .zIndex(4)
                .build();
    }

    /**
     * Création de la bulle avec un {@link EntityComponent}
     *
     * @param data x, y
     * @return la bulle de tchat
     */
    @Spawns("drop")
    public Entity newDroppedArme(SpawnData data) {
        return FXGL.entityBuilder().from(data)
                .type(EntityType.DROP)
                .with(new EntityComponent())
                .zIndex(3).build();
    }
}
