package fr.projetjava.client.ui.menu;

import com.almasb.fxgl.app.scene.FXGLMenu;
import com.almasb.fxgl.app.scene.MenuType;
import com.almasb.fxgl.dsl.FXGL;
import com.almasb.fxgl.scene.Scene;
import fr.projetjava.client.Main;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.StringBinding;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import org.jetbrains.annotations.NotNull;

public class MainMenu extends FXGLMenu {

    /**
     * Création du menu principale
     */
    public MainMenu() {
        super(MenuType.MAIN_MENU);

        AsteroidsButton button = new AsteroidsButton("Solo", () -> {
            Main.getInstance().startSolo();
            fireNewGame();
        });

        button.setTranslateX(FXGL.getAppWidth() / 2 - 100);
        button.setTranslateY(FXGL.getAppHeight() / 2 - 120);


        AsteroidsButton button2 = new AsteroidsButton("Multiplayer", () -> {
            Main.getInstance().startMulti();
            fireNewGame();
        });


        button2.setTranslateX(FXGL.getAppWidth() / 2 - 100);
        button2.setTranslateY(FXGL.getAppHeight() / 2 - 20);


        getMenuContentRoot().getChildren().add(button);
        getMenuContentRoot().getChildren().add(button2);
    }

    /**
     * Appellé quand il rentre dans le menu
     *
     * @param prevState
     */
    @Override
    public void onEnteredFrom(@NotNull Scene prevState) {
        fr.projetjava.server.Main.stop();
        if (Main.getInstance().getServer() != null)
            Main.getInstance().getServer().stop();
        Main.getInstance().getEntities().clear();
        FXGL.getGameWorld().reset();

        //GEstion du sound
        //FXGL.getAudioPlayer().loopMusic(FXGL.getAssetLoader().loadMusic("ambiance.mp3"));
        //FXGL.getAudioPlayer().stopMusic(FXGL.getAssetLoader().loadMusic("ambiance3.mp3"));
        //ENTRE DANS LE JEU / FINI UNE PARTIE
    }

    /**
     * @param stringBinding
     * @param runnable
     * @return
     */
    @NotNull
    @Override
    protected Button createActionButton(@NotNull StringBinding stringBinding, @NotNull Runnable runnable) {
        return new Button();
    }

    /**
     * @param s
     * @param runnable
     * @return
     */
    @NotNull
    @Override
    protected Button createActionButton(@NotNull String s, @NotNull Runnable runnable) {
        return new Button();
    }

    /**
     * @param v
     * @param v1
     * @return
     */
    @NotNull
    @Override
    protected Node createBackground(double v, double v1) {
        return new ImageView(FXGL.getAssetLoader().loadImage("TheJava.png"));
    }

    /**
     * @param s
     * @return
     */
    @NotNull
    @Override
    protected Node createProfileView(@NotNull String s) {
        return new Text();
    }

    /**
     * @param s
     * @return
     */
    @NotNull
    @Override
    protected Node createTitleView(@NotNull String s) {
        return new Text();
    }

    /**
     * @param s
     * @return
     */
    @NotNull
    @Override
    protected Node createVersionView(@NotNull String s) {
        return new Text();
    }

    /**
     * Les boutons multi / solo
     */
    private static class AsteroidsButton extends StackPane {
        public AsteroidsButton(String name, Runnable action) {

            Rectangle bg = new Rectangle(200, 40);
            bg.setStroke(Color.WHITE);

            Text text = FXGL.getUIFactory().newText(name, Color.WHITE, 18);

            bg.fillProperty().bind(
                    Bindings.when(hoverProperty()).then(Color.WHITE).otherwise(Color.BLACK)
            );

            text.fillProperty().bind(
                    Bindings.when(hoverProperty()).then(Color.BLACK).otherwise(Color.WHITE)
            );

            setOnMouseClicked(e -> action.run());

            getChildren().addAll(bg, text);
        }
    }
}
