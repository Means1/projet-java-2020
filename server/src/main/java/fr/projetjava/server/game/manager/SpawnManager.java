package fr.projetjava.server.game.manager;

import fr.projetjava.common.CollisionManager;
import fr.projetjava.common.network.packet.SpawnEntityPacket;
import fr.projetjava.server.entity.ServerEntity;
import fr.projetjava.server.entity.Target;
import fr.projetjava.server.entity.living.EnnemiServerEntity;
import fr.projetjava.server.game.Game;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class SpawnManager {

    Game game;
    @Getter
    @Setter
    private int cap;
    @Getter
    @Setter
    private int level;

    public SpawnManager(Game game) {
        this.game = game;
    }

    /**
     * Thread qui gère le spawn des mobs
     */
    public void start() {
        new Thread(() -> {
            while (true) {

                long count = game.getEntities().stream().filter(p -> p instanceof EnnemiServerEntity).count();

                //S'il y a pas assez d'entité, on en refait spawn
                if (count < cap) {
                    ArrayList<SpawnEntityPacket.SpawnEntity> list = new ArrayList<>();
                    //On fait spawn la moitier pour éviter de tout faire spawn d'un coup
                    for (int i = 0; i < (cap - count) / 2; i++) {

                        EnnemiServerEntity ennemiServerEntity = new EnnemiServerEntity(game, Math.max(1, level - 2 + new Random().nextInt(5)));
                        int x = 0;
                        int y = 0;

                        List<ServerEntity> entity = game.entities.stream().filter(p -> p instanceof Target).collect(Collectors.toList());

                        while (!check(x, y, entity)){
                            x = new Random().nextInt(CollisionManager.getResult().length);
                            y = new Random().nextInt(CollisionManager.getResult()[0].length);
                        }

                        ennemiServerEntity.setX(x);
                        ennemiServerEntity.setY(y);
                        game.getEntities().add(ennemiServerEntity);
                        list.add(ennemiServerEntity.getPacket());
                    }

                    SpawnEntityPacket packet = new SpawnEntityPacket(list.toArray(SpawnEntityPacket.SpawnEntity[]::new));
                    game.players.forEach(p -> p.getClientProcessor().send(packet));
                }

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public boolean check(int x, int y, List<ServerEntity> entities) {
        return CollisionManager.checkPosition(x + 10, y, 40, 62) && entities.stream().noneMatch(p -> (Math.pow(p.getX() - x, 2) + Math.pow(p.getY() - y, 2)) < 50000);
    }
}
