package fr.projetjava.server.game;

import fr.projetjava.common.network.packet.LaunchPacket;
import fr.projetjava.common.network.packet.LoginPacket;
import fr.projetjava.common.network.packet.RemoveEntityPacket;
import fr.projetjava.common.network.packet.SpawnEntityPacket;
import fr.projetjava.server.Main;
import fr.projetjava.server.entity.ServerEntity;
import fr.projetjava.server.entity.living.PlayerServerEntity;
import fr.projetjava.server.entity.living.VipServerEntity;
import fr.projetjava.server.entity.other.MapServerEntity;
import fr.projetjava.server.game.manager.PositionManager;
import fr.projetjava.server.game.manager.SpawnManager;
import fr.projetjava.server.game.manager.VipServerManager;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

@Getter
public class Game {


    private final Thread t;
    private final PositionManager positionManager;
    public ArrayList<ServerEntity> entities = new ArrayList<>();
    public ArrayList<PlayerServerEntity> players = new ArrayList<>();
    public boolean runnning = false;
    @Setter
    public boolean endGame = false;
    @Getter
    private SpawnManager spawnManager;
    @Getter
    private VipServerManager vipServerManager;


    /**
     * Une partie
     */
    public Game() {
        //Initialisation des managers
        positionManager = new PositionManager(this);
        Main.getInstance().getGames().add(this);
        entities.add(new MapServerEntity(this));
        entities.add(new VipServerEntity(this));

        //Compte a rebourd
        t = new Thread(() -> {
            try {
                int i = 10;
                while (i >= -1 && !endGame) {
                    int finalI = i;
                    this.players.forEach(p -> p.getClientProcessor().send(new LaunchPacket(finalI)));

                    if (i == 0) {
                        runnning = true;
                        new Thread(() -> {
                            try {
                                Thread.sleep(25000);
                            } catch (InterruptedException e) {
                            }
                            start();
                        }).start();
                    }
                    i--;
                    if (this.players.size() == 0) {
                        i = 10;
                    }
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            } catch (Throwable e) {
                e.printStackTrace();
            }
        });
        t.start();
    }

    /**
     * Lancement de la partie
     */
    public void start() {
        this.vipServerManager = new VipServerManager(this);
        this.spawnManager = new SpawnManager(this);
        this.spawnManager.setCap(50);
        this.spawnManager.setLevel(1);
        this.spawnManager.start();
        this.vipServerManager.start();
    }

    /**
     * Appellé quand un nouveau joueur se connecte
     * @param playerEntity
     */
    public void addPlayer(PlayerServerEntity playerEntity){
        playerEntity.setX(2330);
        playerEntity.setY(2250);
        //On lui dit son ID
        playerEntity.getClientProcessor().send(new LoginPacket(playerEntity.getId()));

        //On lui donne la position de toutes les entitées déjà spawn
        SpawnEntityPacket.SpawnEntity[] spawnEntities = new ArrayList<>(this.entities).stream().map(p -> p.getPacket()).toArray(SpawnEntityPacket.SpawnEntity[]::new);
        playerEntity.getClientProcessor().send(new SpawnEntityPacket(spawnEntities));

        this.players.add(playerEntity);

        //On dit aux autres players ou il se situe
        new ArrayList<>(this.players).forEach(p -> p.getClientProcessor().send(
                new SpawnEntityPacket(
                        new SpawnEntityPacket.SpawnEntity[]{
                                playerEntity.getPacket()
                        })));
        this.entities.add(playerEntity);
    }

    /**
     * Appellé quand un joueur quitte la partie
     * @param entityByClientPorcessor
     */
    public void leftPlayer(PlayerServerEntity entityByClientPorcessor) {
        this.entities.remove(entityByClientPorcessor);
        this.players.remove(entityByClientPorcessor);
        //On enleve l'entité pour tous les joueurs connectés
        if (this.players.size() == 0 && isRunnning()){
            stop();
        } else {
            this.players.forEach(p -> p.getClientProcessor().send(new RemoveEntityPacket(entityByClientPorcessor.getId())));
        }
    }

    public void stop(){
        Main.getInstance().getGames().remove(this);
    }
}
