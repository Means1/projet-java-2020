package fr.projetjava.server.entity.living;

import fr.projetjava.common.CollisionManager;
import fr.projetjava.common.entity.EntityType;
import fr.projetjava.common.network.packet.MoveEntityPacket;
import fr.projetjava.common.utils.Direction;
import fr.projetjava.common.utils.MouvementDirection;
import fr.projetjava.server.entity.CanMove;
import fr.projetjava.server.entity.Target;
import fr.projetjava.server.game.Game;
import fr.projetjava.server.graph.Node;
import lombok.Getter;

import java.awt.*;


public class VipServerEntity extends LivingServerEntity implements Target, CanMove {

    @Getter
    Node objectif;
    Node currentNode;
    private final Direction direction = new Direction();
    private boolean stop;

    /**
     * Création d'un vip côté serveur
     *
     * @param game
     */
    public VipServerEntity(Game game) {
        super(EntityType.VIP, game, new Rectangle(16, 0, 32, 48), 100);

        //coordonnée de base
        setX(2180);
        setY(2250);
    }

    /**
     * A chaque update, on défini les coordonnées et son mouvement si néccessaire
     */
    @Override
    public void processMouvement() {
        if (objectif == null)
            return;
        if (!direction.isNul()) {
            double value = Math.sqrt(Math.pow(direction.getNorth(), 2) + Math.pow(direction.getRight(), 2));
            if (value != 0) {
                if (CollisionManager.checkPosition((int) (getBbox().getX() + getX() + (direction.getRight() / value) * 0.5),
                        (int) (getBbox().getY() + getY() - (direction.getNorth() / value) * 0.5),
                        (int) getBbox().getWidth(), (int) getBbox().getHeight())) {
                    setX(getX() + 0.5 *(direction.getRight()/value));
                    setY(getY() - 0.5 *(direction.getNorth()/value));
                } else if (CollisionManager.checkPosition((int) (getBbox().getX()+getX()),
                        (int) (getBbox().getY()+getY() - (direction.getNorth()/value) * 0.5),
                        (int) getBbox().getWidth(), (int) getBbox().getHeight())){
                    setY(getY() - 0.5 *(direction.getNorth()/value));
                } else if (CollisionManager.checkPosition((int) (getBbox().getX()+getX() + (direction.getRight()/value) * 0.5),
                        (int) (getBbox().getY()+getY()),
                        (int) getBbox().getWidth(), (int) getBbox().getHeight())){
                    setX(getX() + 0.5 *(direction.getRight()/value));
                }
            }
        }
        //S'il y a pas de chemin ou il est a 20 de distance du target
        if (currentNode == null || distance() < 20){
            if (currentNode == objectif){
                getGame().getVipServerManager().finishRound();
                notifyMouvement();
            } else {
                if (currentNode == null){
                    currentNode = objectif.getShortestPath().get(0);
                    notifyMouvement();
                } else {
                    Node next = getNext(currentNode);
                    if (next == null) {
                        currentNode = objectif;
                        notifyMouvement();
                    } else {
                        currentNode = next;
                        notifyMouvement();
                    }
                }
            }
        }
    }

    private double distance() {
        return Math.sqrt(Math.pow(getX() - currentNode.getX(), 2) + Math.pow(getY() - currentNode.getY(), 2));
    }


    /**
     * Calcul de la direction pour le mouvement
     */
    private void notifyMouvement() {
        MoveEntityPacket packet = new MoveEntityPacket();
        packet.setId(this.getId());
        MouvementDirection mouvementDirection = new MouvementDirection(getX(), getY(), 0.25, direction);
        if (currentNode == null || stop) {
            direction.setRight(0);
            direction.setNorth(0);
        } else {
            direction.setRight(currentNode.getX() - getX());
            direction.setNorth(getY() - currentNode.getY());
        }
        packet.setMouvementDirection(mouvementDirection);
        getGame().players.forEach(p -> p.getClientProcessor().send(packet));
    }

    /**
     * Récupère la prochaine node pour le déplacement
     *
     * @param node
     * @return
     */
    private Node getNext(Node node) {
        for (int i = 0; i < objectif.getShortestPath().size(); i++) {
            if (objectif.getShortestPath().get(i).equals(node)) {
                if (i + 1 < objectif.getShortestPath().size())
                    return objectif.getShortestPath().get(i + 1);
            }
        }
        return null;
    }

    /**
     * Met en pause ou non le vip
     */
    public void toggleStop() {
        stop = !stop;
        notifyMouvement();
    }

    /**
     * Définit l'objetif
     *
     * @param objectif l'objectif
     */
    public void setObjectif(Node objectif) {
        this.objectif = objectif;
        this.currentNode = null;
    }
}
