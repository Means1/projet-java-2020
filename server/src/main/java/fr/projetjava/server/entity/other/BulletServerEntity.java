package fr.projetjava.server.entity.other;

import fr.projetjava.common.ArmeType;
import fr.projetjava.common.entity.EntityType;
import fr.projetjava.common.utils.MouvementDirection;
import fr.projetjava.server.entity.ServerEntity;
import fr.projetjava.server.game.Game;
import lombok.Getter;
import lombok.Setter;

import java.awt.*;

@Getter
@Setter
/**
 * Entité qui représrnte une fireball ou un tir d'une arme
 */
public class BulletServerEntity extends ServerEntity {

    private ArmeType armeType;
    private MouvementDirection mouvementDirection;
    private int life;

    public BulletServerEntity(EntityType type, Game game, ArmeType armeType, MouvementDirection mouvementDirection) {
        super(type, game, new Rectangle());
        this.armeType = armeType;
        mouvementDirection.setSpeed(armeType.getSpeed());
        this.mouvementDirection = mouvementDirection;
        this.life = armeType.getLife();
    }
}
