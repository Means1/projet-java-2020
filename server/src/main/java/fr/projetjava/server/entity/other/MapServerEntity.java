package fr.projetjava.server.entity.other;

import fr.projetjava.common.entity.EntityType;
import fr.projetjava.server.entity.ServerEntity;
import fr.projetjava.server.game.Game;

import java.awt.*;

/**
 * Entité qui représente la map
 */
public class MapServerEntity extends ServerEntity {
    public MapServerEntity(Game game) {
        super(EntityType.MAP, game, new Rectangle());
    }
}
