package fr.projetjava.server.graph;

import lombok.Getter;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

@Getter
/**
 * Méthode de djisktra
 */
public class Graph {

    public static Graph graph;

    static {
        graph = new Graph();
        Node nodeVoiture = new TargetNode("Voiture",2026,2096, new String[]{"Oups, j'ai oublié un\npapier dans ma voiture !", "J'ai oublié mon déjeuner\ndans ma voiture,\nen plus c'est un gateau."});
        Node nodeB = new Node("B",1520,2096);
        Node nodeC = new Node("C", 1520, 1598);
        Node nodeAkim = new TargetNode("Akim", 1633, 1484, new String[]{"J'ai besoin de sentir\nl'odeur des frites de\nchez Akim !"});
        Node nodeCommandeAkim = new TargetNode("CommandeAkim", 1471, 1362, new String[]{"Je vais me commander un\npetit crudités chez Akim !", "J'ai envie d'un petit\nKitKat."});
        Node nodeD = new Node("D",1776,1390);
        Node nodeToilletteAkim = new Node("ToilletteAkim",1982,1390);
        Node nodeChezMireille = new TargetNode("ChezMireille",1982,1674, new String[]{"Il faut que je vois la\npatronne.", "Il faut que je demande\nà Mireille si elle sera là\npendant les Fintz d'Or."});
        Node nodePorteAtilla = new Node("PorteAtilla",2333,1384);
        Node nodeEntreeAtilla = new Node("EntreeAtilla",2596,1372);
        Node nodeBureauAtilla = new TargetNode("BureauAtilla",2596,1066, new String[]{"Je vais voir les geeks !", "Je vais leurs taxer de la\npizza.", "Je vais taxer des crèpes\nà Atilla. ", "Je vais aller embêter\nLoic, le meilleur\nprésident d'Atilla !"});
        Node nodeReprographie = new Node("Reprographie",2327,893);
        Node nodeHallCauchy = new TargetNode("HallCauchy",2414,435,new String[]{"Je vais accueillir les\nnouveaux arrivants,\nj'espère qu'ils vont\nprendre visual computing\nen ING3 !"});
        Node nodeCafeMachine = new TargetNode("CafeMachine",2056,891,new String[]{"Je vais me prendre un\npetit expresso ou un Coca..."});
        Node nodePorteAmphi = new Node("PorteAmphi",1959,730);
        Node nodeJ = new Node("J",1810,854);
        Node nodeTableauAmphi = new TargetNode("TableauAmphi",1924,968, new String[]{"Mince, je suis en retard\npour mon cours !", "Je dois aller faire une\nprésentation en amphi !"});
        Node nodeE = new Node("E",2167,541);
        Node nodeF = new Node("F",2167,419);
        Node nodePorte112 = new Node("Porte112",1741,413);
        Node nodeArtScene = new TargetNode("ArtScene",1757,210, new String[]{"Allons écouter de la\nmusiqueeeeeeee !\nJ'espère que Judith \nsera là !"});
        Node nodeEntree112 = new Node("Entree112",1613,414);
        Node nodeTableau112 = new TargetNode("Tableau112",1601,208, new String[]{"Et c'est parti pour un\ncours en CPI SANS PC !", "Allons faire du Pascal sur\nfeuille ! Je préfère quand\nmême faire du O C A M L !\nEn plus OCAML c'est juste\npour l'argent..."});
        Node nodeG = new Node("G",1418,537);
        Node nodeToillette113 = new Node("Toilette113",1418,817);
        Node nodePorte113 = new Node("Porte113",1288,817);
        Node nodeH = new Node("H",1288,537);
        Node nodeTableau113 = new TargetNode("Tableau113", 1098, 549, new String[]{"Et c'est parti pour un\ncours en CPI SANS PC", "Allons faire du Pascal sur\nfeuille ! Je préfère quand\nmême faire du O C A M L !\nEn plus OCAML c'est juste\npour l'argent..."});
        Node nodeFond113 = new TargetNode("Fond113", 1098, 1093, new String[]{"AHH j'ai entendu un\nbruit !!! Je dois aller\ntaxer les CPI avec leurs\nTic Tacs !"});
        Node nodeI = new Node("I", 1411, 1109);
        Node nodeAkimBis = new Node("AkimBis",1303,1216);
        Node nodeArbreDevant = new TargetNode("ArbreDevant",749,1635, new String[]{"On va écouter la nature !\nCa fait pas de mal !"});
        Node nodeAB = new Node("AB",474,1782);
        Node nodeAC = new Node("AC",200,1709);
        Node nodeAD = new Node("AD",194,1307);
        Node nodeObserveOiseau = new TargetNode("ObserveOiseau",193,1275, new String[]{"J'ai vu oiseau !!!\n Viteeee !"});
        Node nodeAF = new Node("AF",912,1298);
        Node nodeAG = new Node("AG",1105,1336);
        nodeVoiture.addNextNode(nodeB);
        nodeB.addNextNode(nodeC);
        nodeB.addNextNode(nodeArbreDevant);
        nodeArbreDevant.addNextNode(nodeAB);
        nodeAB.addNextNode(nodeAC);
        nodeAC.addNextNode(nodeAD);
        nodeAD.addNextNode(nodeObserveOiseau);
        nodeAF.addNextNode(nodeArbreDevant);
        nodeAF.addNextNode(nodeAG);
        nodeAG.addNextNode(nodeAkimBis);
        nodeB.addNextNode(nodeArbreDevant);
        nodeC.addNextNode(nodeAkim);
        nodeAkim.addNextNode(nodeCommandeAkim);
        nodeAkim.addNextNode(nodeD);
        nodeD.addNextNode(nodeToilletteAkim);
        nodeToilletteAkim.addNextNode(nodeChezMireille);
        nodeToilletteAkim.addNextNode(nodePorteAtilla);
        nodePorteAtilla.addNextNode(nodeEntreeAtilla);
        nodeEntreeAtilla.addNextNode(nodeBureauAtilla);
        nodePorteAtilla.addNextNode(nodeReprographie);
        nodeReprographie.addNextNode(nodeHallCauchy);
        nodeReprographie.addNextNode(nodeCafeMachine);
        nodeHallCauchy.addNextNode(nodeCafeMachine);
        nodeHallCauchy.addNextNode(nodeE);
        nodeReprographie.addNextNode(nodeE);
        nodeCafeMachine.addNextNode(nodeE);
        nodeHallCauchy.addNextNode(nodeF);
        nodeE.addNextNode(nodeF);
        nodeE.addNextNode(nodePorteAmphi);
        nodeCafeMachine.addNextNode(nodePorteAmphi);
        nodeHallCauchy.addNextNode(nodePorteAmphi);
        nodePorteAmphi.addNextNode(nodeJ);
        nodeJ.addNextNode(nodeTableauAmphi);
        nodeF.addNextNode(nodePorte112);
        nodePorte112.addNextNode(nodeArtScene);
        nodePorte112.addNextNode(nodeEntree112);
        nodeEntree112.addNextNode(nodeTableau112);
        nodeE.addNextNode(nodeG);
        nodeG.addNextNode(nodeToillette113);
        nodeToillette113.addNextNode(nodePorte113);
        nodePorte113.addNextNode(nodeH);
        nodeH.addNextNode(nodeTableau113);
        nodeTableau113.addNextNode(nodeFond113);
        nodeToillette113.addNextNode(nodeI);
        nodeI.addNextNode(nodeAkimBis);
        nodeCommandeAkim.addNextNode(nodeAkimBis);
        graph.addNode(nodeVoiture);
        graph.addNode(nodeB);
        graph.addNode(nodeArbreDevant);
        graph.addNode(nodeAB);
        graph.addNode(nodeAC);
        graph.addNode(nodeAD);
        graph.addNode(nodeObserveOiseau);
        graph.addNode(nodeAF);
        graph.addNode(nodeAG);
        graph.addNode(nodeC);
        graph.addNode(nodeAkim);
        graph.addNode(nodeCommandeAkim);
        graph.addNode(nodeD);
        graph.addNode(nodeToilletteAkim);
        graph.addNode(nodeChezMireille);
        graph.addNode(nodePorteAtilla);
        graph.addNode(nodeEntreeAtilla);
        graph.addNode(nodeBureauAtilla);
        graph.addNode(nodeReprographie);
        graph.addNode(nodeHallCauchy);
        graph.addNode(nodeCafeMachine);
        graph.addNode(nodeE);
        graph.addNode(nodeF);
        graph.addNode(nodePorteAmphi);
        graph.addNode(nodeJ);
        graph.addNode(nodeTableauAmphi);
        graph.addNode(nodePorte112);
        graph.addNode(nodeArtScene);
        graph.addNode(nodeTableau112);
        graph.addNode(nodeG);
        graph.addNode(nodeToillette113);
        graph.addNode(nodePorte113);
        graph.addNode(nodeH);
        graph.addNode(nodeTableau113);
        graph.addNode(nodeFond113);
        graph.addNode(nodeI);
        graph.addNode(nodeAkimBis);
    }

    private final Set<Node> nodes = new HashSet<>();


    public void addNode(Node node) {
        nodes.add(node);
    }

    public static Node getLowestDistanceNode(Set<Node> unsettledNodes) {
        Node lowestDistanceNode = null;
        double lowestDistance = Double.MAX_VALUE;
        for (Node node : unsettledNodes) {
            double nodeDistance = node.getDistance();
            if(nodeDistance < lowestDistance){
                lowestDistance = nodeDistance;
                lowestDistanceNode = node;
            }
        }
        return lowestDistanceNode;
    }

    private static void calculateMinimumDistance(Node evaluationNode,
                                                 Double poid, Node noeudSource) {
        Double sourceDistance = noeudSource.getDistance();
        if (sourceDistance + poid < evaluationNode.getDistance()) {
            evaluationNode.setDistance(sourceDistance + poid);
            LinkedList<Node> shortestPath = new LinkedList<>(noeudSource.getShortestPath());
            shortestPath.add(noeudSource);
            evaluationNode.setShortestPath(shortestPath);
        }
    }

    public static Node calculeCheminLePlusCourt(String sourceName, String fin){
        graph.nodes.forEach(p -> {
            p.setDistance(Double.MAX_VALUE);
            p.getShortestPath().clear();
        });

        Node source = graph.nodes.stream().filter(p -> p.getName().equals(sourceName)).findFirst().orElse(null);
        if (source == null)
            return null;

        source.setDistance(0.0);

        Set<Node> settledNodes = new HashSet<>();
        Set<Node> unsettledNodes = new HashSet<>();

        unsettledNodes.add(source);

        while ( unsettledNodes.size() != 0){
            Node noeudCourant = getLowestDistanceNode(unsettledNodes);
            unsettledNodes.remove(noeudCourant);
            for (Map.Entry<Node,Double> adjacencyPair:
                    noeudCourant.getAdjacentNodes().entrySet()){
                Node noeudAdjacent = adjacencyPair.getKey();
                Double poid = adjacencyPair.getValue();
                if (!settledNodes.contains(noeudAdjacent)){
                    calculateMinimumDistance(noeudAdjacent,poid,noeudCourant);
                    unsettledNodes.add(noeudAdjacent);
                }
            }
            settledNodes.add(noeudCourant);
        }
        Node node= graph.getNodes().stream().filter(p -> p.getName().equals(fin)).findFirst().orElse(null);
        return node;
    }
}
